#include <Arduino.h>
#include <SPI.h>
#include <Adafruit_GFX.h>
#include "Adafruit_SSD1306.h"

#include <EEPROM.h>
#define INITIAL_RESET_EEPROM  true

#define POTI    A3      // change char position
#define BUTTON1 A2      // char up (send now)
#define BUTTON2 12      // char down (read next bytes)

#define SERVER_URL "http://server-ip-or-domain:"
#define SERVER_PORT "61080"
#define SERVER_PATH "/what/"

// SPI: DC, RST, DS
Adafruit_SSD1306 oled(A1, 10, A0);

#define BUFLEN  147

char text[43] = 
    "KREFELD              " // 21 chars
    "                     ";

uint16_t bytepos = 0;

uint8_t txtid = 0;
char        c = 0;

uint8_t pos = 21;
uint8_t endpos = 0;

uint8_t  mode = 0;
uint16_t ticker = 0;

void requestsend(void) {
  oled.fillRect(0, 8, 128, 56, BLACK);
  txtid = 0;
  oled.setCursor(0,8);
  Serial.println(F("at+httpterm"));
  mayGetSome(500);    
  oled.display();
  Serial.println(F("at+httpinit"));
  mayGetSome(500);    
  oled.display();
  Serial.println(F("at+httppara=\"CID\",1"));
  mayGetSome(500);
  oled.display();
  Serial.print(F("at+httppara=\"URL\",\""));

  Serial.print(F(SERVER_URL));
  Serial.print(F(SERVER_PORT));
  Serial.print(F(SERVER_PATH));
  Serial.print(bytepos);
  Serial.print(F("/"));
  
  for (txtid = 20; txtid>0; --txtid) {
    if(text[txtid] != ' ') { // to remove/ignore spaces from the end
      endpos = txtid+1;
      txtid = 1; // next -- will set it to 0 and loop stops
    }
  }
  for (txtid = 0; txtid<endpos; ++txtid) Serial.print(text[txtid]);
  clearText();
  Serial.println(F("\""));
  txtid = 0;
  oled.setCursor(0,8);
  mayGetSome(300);
  oled.display();
  
  Serial.println(F("at+httpaction=0"));
  oled.setCursor(0,8);
  mayGetSome(2000);
  oled.display();
  oled.fillRect(0, 8, 128, 56, BLACK);
  txtid = 0;
  oled.setCursor(0,8);
  oled.display();
  Serial.println(F("at+httpread"));
  mode = 2;
}

inline void clearText() {
  for (txtid = 21; txtid<42; ++txtid) text[txtid] = ' ';
  txtid = 0;
}

void mayGetSome(int ms) {
  while (ms > 0) {
    while ( Serial.available() ) {
      c = Serial.read();
      if (c == '\r') continue;
      if (c == '\0') c = ' ';
      if (c == '\n') c = ' ';
      oled.print(c);
      txtid = (txtid+1)%BUFLEN;
      if (txtid==0) oled.setCursor(0,8);
    }
    delay(1);
    ms -= 1;
  }
}

void setup() {
  Serial.begin(9600);
  oled.begin();

  pinMode(BUTTON1, INPUT_PULLUP);
  pinMode(BUTTON2, INPUT_PULLUP);
   
  oled.clearDisplay();
  oled.setTextSize(1);
  oled.setTextColor(WHITE);
  oled.setCursor(0,0);
  oled.print(F("init"));
  oled.display();
  
  if (INITIAL_RESET_EEPROM == true) EEPROM.put(0, text);
  EEPROM.get(0, text);
  
  // gsm modem reset/init
  Serial.println(F("atz"));
  oled.setCursor(0,8);
  mayGetSome(2000);
  oled.display();
  txtid = 42;
  // gsm modem shold be more silent (echo 0)
  Serial.println(F("ate0"));
  for (int ticker = 0; ticker < 10; ++ticker) {
    mayGetSome(1300);
    oled.print(F("."));
    oled.display();
  }
  txtid = 0;
  // gsm init should be done. try gprs mode:
  Serial.println(F("at+sapbr=1,1"));
  for (int ticker = 0; ticker < 5; ++ticker) {
    mayGetSome(500);
    oled.print(F("."));
    oled.display();
  }
  txtid = 0;
  ticker = 0;
}

void loop() {
  ticker = (ticker+1)%5000; // for a display refresh (sometimes)
  
  if (mode == 0) { // ---------------------------------------- set
    pos = map(analogRead(POTI),0,1024,0,22);

    if (ticker%200 == 0) { // ---------------- change pos
      oled.clearDisplay();
      oled.setCursor(0,0);
      
      for (txtid = 21; txtid<42; ++txtid) {
        text[txtid] = ' ';
        if ((pos < 21) && (txtid == pos+21)) text[txtid] = '^';
      }
      oled.print(text);
      oled.display();
    }

    if (pos > 20) { // ---------------------- ask send
      oled.setCursor(32,32);
      oled.fillRect(30, 26, 88, 24, BLACK);
      oled.print(F("SEND NOW?"));
      
      oled.setCursor(100,28);
      oled.print(F("yes"));
      oled.setCursor(106,36);
      oled.print(F("no"));
      oled.display();

      if (digitalRead(BUTTON1) == LOW) {
        delay(200);
        EEPROM.put(0, text);
        mode = 1;
      }

    } else { // ------------------------ change character
    
      if (digitalRead(BUTTON1) == LOW) {
        delay(100);
        if (digitalRead(BUTTON2) == LOW) {
          // ------------------------------- press both, clear rest
          for (txtid = pos; txtid<21; ++txtid) {
            text[txtid] = ' ';
          }
        } else {
          text[pos] = text[pos]+1;
          if (text[pos] > '_') text[pos] = ' ';
          if (text[pos] == '[') text[pos] = '^';
          if (text[pos] == '"') text[pos] = '(';
          if (text[pos] == '/') text[pos] = '0';
          if (text[pos] == ';') text[pos] = '@';
        }
        oled.clearDisplay();
        oled.setCursor(0,0);
        for (txtid = 21; txtid<42; ++txtid) {
          text[txtid] = ' ';
          if ((pos < 21) && (txtid == pos+21)) text[txtid] = '^';
        }
        oled.print(text);
        oled.display();
      }
      
      if (digitalRead(BUTTON2) == LOW) {
        delay(200);
        if (digitalRead(BUTTON1) == LOW) {
          // ------------------------------- press both, clear rest
          for (txtid = pos; txtid<21; ++txtid) {
            text[txtid] = ' ';
          }
        } else {
          text[pos] = text[pos]-1;
          if (text[pos] < ' ') text[pos] = '_';
          if (text[pos] == ']') text[pos] = 'Z';
          if (text[pos] == '?') text[pos] = ':';
          if (text[pos] == '/') text[pos] = '.';
          if (text[pos] == 39) text[pos] = '!';
        }
        oled.clearDisplay();
        oled.setCursor(0,0);
        for (txtid = 21; txtid<42; ++txtid) {
          text[txtid] = ' ';
          if ((pos < 21) && (txtid == pos+21)) text[txtid] = '^';
        }
        oled.print(text);
        oled.display();
      }

    }
  
  } else if (mode == 1) { // ----------------------- start request
    bytepos = 0;
    requestsend();

  } else if (mode == 2) { // ----------------------- read request
    
    while ( Serial.available() ) {
      c = Serial.read();
      if (c == '\r') continue;
      if (c == -61) continue; // symbol before utf-8
      if (c == -62) continue; // other symbol before utf-8
      if (c == '\0') {
        c = ' ';
      } else if (c == '\n') {
        c = ' ';
      } else if (c == -97) {
        c = 224; // ß
      } else if (c == -80) {
        c = 248; // °
      } else if (c == -67) {
        c = 171; // 1/2
      } else if (c == -78) {
        c = 253; // ²
      } else if (c == -92) {
        c = 132; // ä
      } else if (c == -74) {
        c = 148; // ö
      } else if (c == -68) {
        c = 129; // ü
      } else if (c == -124) {
        c = 142; // Ä
      } else if (c == -106) {
        c = 153; // Ö
      } else if (c == -100) {
        c = 154; // Ü
      } else if (c == -85) {
        c = 0xAE; // <<
      } else if (c == -69) {
        c = 0xAF; // >>
      }
      oled.print(c);
      txtid = (txtid+1)%BUFLEN;
      if (txtid==0) oled.setCursor(0,8);
    }
    
    if (digitalRead(BUTTON1) == LOW) {
      delay(200);
      mode = 0;
    }
    
    if (digitalRead(BUTTON2) == LOW) {
      delay(200);
      bytepos += 100;
      requestsend();
    }
  }

  if (ticker==0) oled.display();
}

