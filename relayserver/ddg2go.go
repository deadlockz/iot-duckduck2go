package main

import (
    "fmt"
    "net/http"
    "net/url"
    "crypto/tls"
    "io/ioutil"
    "strings"
    "encoding/json"
    "strconv"
)

const DDG_LANG = "de"
const LISTEN_PORT = "61080"
const LISTEN_PATH = "/what/"

// types to get json in a better way

type RelatedTopic struct {
    Text        string  `json:"Text"`
    FirstURL    string  `json:"FirstURL"`
}

type Result struct {
    Abstract        string          `json:"Abstract"`
    RelatedTopics   []RelatedTopic  `json:"RelatedTopics"`
}

func sayDuck(w http.ResponseWriter, r *http.Request) {
    message := r.URL.Path
    message = strings.TrimPrefix(message, LISTEN_PATH)
    messages := strings.Split(message, "/")
    if (len(messages) == 2) {
        // we use the "request" part as message to duckduckgo
        message = messages[1]
    }
    message = "https://api.duckduckgo.com/?format=json&q=" + 
        url.QueryEscape(message)
    
    // we can live with invalid ssl certs!
    client := &http.Client{
        Transport: &http.Transport{
            TLSClientConfig: &tls.Config{
                InsecureSkipVerify: true,
            },
        },
    }
    req, _ := http.NewRequest("GET", message, nil)
    req.Header.Add("Accept-Language", DDG_LANG)
    resp, err := client.Do(req)
    
    if (err != nil) {
        panic(err)
    }
    
    defer resp.Body.Close()

    body, err := ioutil.ReadAll(resp.Body)
    if (err != nil) {
        panic(err)
    }

    var result Result
    json.Unmarshal(body, &result)
    html := result.Abstract
    
    // maybe duckduckgo does not find a description for the query
    if (html == "") {
        // we collect some search results
        for key, value := range result.RelatedTopics {
            if (value.Text != "") {
                html += fmt.Sprintf("(%v) %v ", key+1, value.Text)
            }
        }
    }
    
    if (len(messages) == 2) {
        /*
         * now we want to cut the result down and get only the
         * max 100 chars starting at pos
         */
        pos, _ := strconv.Atoi(messages[0])
        if (pos < 0) {
            html = ""
        } else if (len(html) > pos) {
            html = html[pos:]
        } else {
            html = ""
        }
    }
    
    if (len(html) > 100) {
        html = html[:100]
    }
    w.Write([]byte(html))
}

func main() {
    http.HandleFunc(LISTEN_PATH, sayDuck)
    if err := http.ListenAndServe(":" + LISTEN_PORT, nil); err != nil {
        panic(err)
    }
}
