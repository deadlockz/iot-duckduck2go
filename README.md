# IoT - DuckDuck 2 Go

An improved DuckDuckGo (and wikipedia?) device.

![chassis is wood](photo.jpg)

- a SIM800L GSM module to connect to a server via GPRS
- arduino pro mini 8MHz 3.3V
- Lipo 3.7 V to 4.2 V (important: 3.3V GSM modem not
  working and 5V BREAK modem)
- 2 buttons (press = connect to ground)
  - change character: up / down (just big letters and numbers)
  - OR: new or first request / get next 100 bytes
  - press both: clear all chars to the right
- 1 potentiometer
  - to change character position
  - complete to left: Ask to send the request (press button)
- SPI oled 1306 display (feel free to change design to I2C)
- the search request is stored in eeprom (please change the `INITIAL_RESET_EEPROM`
  to `false` in the code after a first start and reprogram)


## You need

- a bit wire
- a small power switch
- 1 arduino pro mini with 8MHz (because it works good on 3,7V)
- usb/uart serial converter (programming with onboard bootloader)
- some male and female "pins" to make gsm modem
  removeable from Tx/Rx, because we need them for programming
- a SIM800L GSM module with a good, flat antenna
- a valid sim card (with a cleared PIN)
- 1 potentiometer (10k, 20k or 25k ... maybe 50k works, too)
- 2 buttons
- a SPI 1306 display
- A SERVER IN THE INTERNET running golang code as relayserver!

## Wireing/circuit

It is a default config:

- Vcc to...
  - Lipo + (and make a switch there)
  - Vcc display
  - Vcc Arduino Pro mini
  - Vcc gsm modem
  - potentiometer right pin (or left)
- GND to...
  - Lipo -
  - GND display
  - GND Arduino Pro mini
  - GND gsm modem
  - button 1
  - button 2
  - potentiometer left pin (or right)
- Arduino Pro mini pins...
  - Tx/Rx to the coresponding GSM modem pins
  - potentiometer middle pin: A3
  - button 1: A2
  - button 2: 12
- display...
  - DC:  A1
  - RST: 10
  - CS:  A0

In the header of `ddphone` you can change some of the pins, but display
SDA (11, MOSI) and SCL (13, clock) are SPI defaults.

On the picture you see a micro and ring LED... you do
not need it in that project, because in GPRS mode no
calls are possible.

## How does it work?

First: Atmega328 has not enough RAM for many applications! Second: We can
improve this, because the most RAM is used to display chars on display
and "the display" acts as memory. That means: we count the number of
chars on the display and only refreshs (end set it to 0) if the display
is full. we do not have to buffer data in a char array, if we do
not "process" this data. Additionaly you get some results from the GSM
modem and display them... I think that is ok for you.

Ok, the GSM modem has many cool features and we can request and get
a whole website... and we can read these data step by step (if we want).
But there are some problems:

- since some years the internet uses httpS (ssl)
- the SSL of the sim800L gsm modem firmware(2016) is limited to TLS1.0
- we can not improve SSL via TCP/IT stack on an 8bit atmel chip
- normal websites are full of HTML stuff... bytes we do not need!
- DuckDuckGo has a JSON interface: Results are still to big (maybe)
- there is a sim800 ssl support: search for `sim800_series_ssl_application_note_v1.01.pdf`

How can we improve this? The answer: a relay server! I build a small
golang server code to handle http (non SSL) requests from the gsm modem
with an additional parameter to allow something like "give me the next
100bytes starting from offset XX". The relay do the https request to
DuckDuckGo and shrink the results down. Is there a wiki entry in the
result, the server sends only that to the gsm modem. Alternativly it
gets the first 10 lines of search results.

The `ddphone.ino` code represents a "IoT client". You have to change
the `SERVER_URL` with your domain or ip, where the golang server will run.
Additional important is `SERVER_PORT`. If you run your own server with a
domain, the default http port 80 is still in use and the well known lower
ports are hard to bind to as non-root user.

The `ddg2go.go` code is the "IoT server" or micro-web-app or just relay
server - feel free to use any good bussiness buzzz word for that. You have
to configure it similar to the client. `LISTEN_PORT` in the const vars
is the most important var. An other var `DDG_LANG` sets the https header
to a non `en-us` language, when server requests data from `api.duckduckgo.com`.
This is a kind of "configure" the duckduckgo api, because the api description
is a bit loosy.

## Hints: init and boot DuckDuck 2 Go (ddphone)

Most problems with gsm modem are...

- a short use of 5Volts (destroy!)
- think everything is initialised but it is not ('cause of bad antenna signal)
- the SIM card contacts are bad

The `setup` routine primary has a focus on the 2nd point. There are many
delays and hints in display to handle problems and waiting to log the
gsm modem into the network. That is an ungly way, because the code does not
handle these problems - you have to read something like `PIN Ready`
... `Call Ready` ... `SMS Ready`. Additionally take a look to the link LED
on the gsm modem. It should do something like this in this order...

- the first 10 seconds it blinks about 12 times: not logged in into network
- off, blink about 2 times with a 2 second delay: logged in successfully
- 3 blinks every second: IP exists, GPRS mode is running!

# Ehhm, is it a beginner project?

Nope, I do not think it is a good start for beginners. You will miss breadboard
images to wire everything or you got problems running golang on a server
or the ports are closed by server firewall. Maybe try a python or apache/php
workaround to make some minimal html server and try to access it by talking
direct to the gsm modem via usb/uart converter. Use a LIPO to power the
modem, because 5V let it burn!

Here is an example code to talk to the modem (do not type the # commends):

~~~
# rest the modem
atz
# make stupid echo mode off
ate0
# gives you some infos about status
ati
# switch to GPRS mode with the default APN/CID 1 beaver
at+sapbr=1,1
# show the IP from the gprs network
at+sapbr=2,1
# try to terminate an old http request (initial you get an error)
at+httpterm
# try an http init
at+httpinit
# set the id, where the http request should run
at+httppara="CID",1
# set the url
at+httppara="URL","http://www.columbia.edu/~fdc/sample.html"
# start the http request
at+httpaction=0
# wait a bit and than read
at+httpread
# terminate the http session to the modem
at+httpterm
~~~

The last command runs me into trouble! The `httpaction` an `httpread` needs
some seconds and if you do not wait and get the data from modem, the
terminate comes to early an you see nothing from the results on the display.
